# Docker: Multi Compose

## Support

Multi Compose is an MIT-licensed open source project. It can grow thanks to the sponsors and support by the amazing backers.

## License

Multi Compose is [MIT licensed](LICENSE).
